# [1.4.0](https://gitlab.com/to-be-continuous/gradle/compare/1.3.1...1.4.0) (2022-05-01)


### Features

* configurable tracking image ([a9131b4](https://gitlab.com/to-be-continuous/gradle/commit/a9131b454cc2fab2344d9719e52193fe1d2cd574))

## [1.3.1](https://gitlab.com/to-be-continuous/gradle/compare/1.3.0...1.3.1) (2022-02-25)


### Bug Fixes

* keep build artifacts regardless of build job status ([fe12c8c](https://gitlab.com/to-be-continuous/gradle/commit/fe12c8cf0881b5b6c130666c9f50ff934c6e7c7d))

# [1.3.0](https://gitlab.com/to-be-continuous/gradle/compare/1.2.1...1.3.0) (2021-12-10)


### Features

* add project root directory variable ([fa0c30a](https://gitlab.com/to-be-continuous/gradle/commit/fa0c30af30a67ad64e28c8b9bb24a2f967d5f20b))

## [1.2.1](https://gitlab.com/to-be-continuous/gradle/compare/1.2.0...1.2.1) (2021-09-03)

### Bug Fixes

* Change boolean variable behaviour ([dfee6cb](https://gitlab.com/to-be-continuous/gradle/commit/dfee6cb401358e46316a1d7e018cfa6bb03897c7))

## [1.2.0](https://gitlab.com/to-be-continuous/gradle/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([ccab558](https://gitlab.com/to-be-continuous/gradle/commit/ccab5588dff93aaed5528a2c10c5aa9a6ddaba47))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/gradle/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([76b028c](https://gitlab.com/Orange-OpenSource/tbc/gradle/commit/76b028cd71852e2e42ef945f5eb556a02214a65d))

## 1.0.0 (2021-05-06)

### Features

* initial release ([4c573b7](https://gitlab.com/Orange-OpenSource/tbc/gradle/commit/4c573b7b8a8017c449cf6ae70e92830d989ceced))
