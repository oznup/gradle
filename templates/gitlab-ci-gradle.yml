# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms
# of the GNU Lesser General Public License as published by the Free Software Foundation;
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================
# default workflow rules
workflow:
  rules:
    # exclude merge requests
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - when: always

variables:
  # variabilized tracking image
  TBC_TRACKING_IMAGE: "$CI_REGISTRY/to-be-continuous/tools/tracking:master"

  GRADLE_IMAGE: "gradle:latest"
  GRADLE_CLI_BIN: "gradle"
  GRADLE_CLI_OPTS: ""
  GRADLE_USER_HOME: $CI_PROJECT_DIR/.gradle
  GRADLE_PROJECT_DIR: "."

  GRADLE_DAEMON: "false"

  GRADLE_BUILD_ARGS: "build"
  GRADLE_PUBLISH_ARGS: "publish"
  GRADLE_DEPENDENCY_CHECK_TASK: "dependencyCheckAnalyze"


stages:
  - build
  - test
  - publish

.gradle-scripts: &gradle-scripts |
  # BEGSCRIPT
  set -e

  function log_info() {
      echo -e "[\\e[1;94mINFO\\e[0m] $*"
  }

  function log_warn() {
      echo -e "[\\e[1;93mWARN\\e[0m] $*"
  }

  function log_error() {
      echo -e "[\\e[1;91mERROR\\e[0m] $*"
  }

  function handle_daemon() {
    mkdir -p "$GRADLE_USER_HOME"
    if [[ ! -f  "$GRADLE_USER_HOME/gradle.properties" ]] || ! grep "^org.gradle.daemon=" "$GRADLE_USER_HOME/gradle.properties" >/dev/null
    then
      log_info "Adding daemon property"
      {
        echo ""
        echo "org.gradle.daemon=$GRADLE_DAEMON"
      } >>  "$GRADLE_USER_HOME/gradle.properties"
    fi
  }

  function handle_executable() {
    if [[ -f "$GRADLE_CLI_BIN" ]]
    then
      log_info "Making $GRADLE_CLI_BIN executable"
      chmod +x "$GRADLE_CLI_BIN"
    fi
  }

  function handle_proxy() {
    proxy_type="$1"
    proxy_value="$2"
    if [[ ! -f  "$GRADLE_USER_HOME/gradle.properties" ]] || ! grep "^systemProp.$proxy_type.proxy" "$GRADLE_USER_HOME/gradle.properties"  >/dev/null
    then
      if [[ -n "$proxy_value" ]]
      then
        log_info "Installing $proxy_type proxy $proxy_value"
        proxy_host=$(echo "$proxy_value" | sed 's%http[s]\?://%%' | awk -F":" '{ print $1; }')
        proxy_port=$(echo "$proxy_value" | sed 's%http[s]\?://%%' | awk -F":" '{ if(NF == 2){ print $2; } else { print "80"; }}')
        {
          echo ""
          echo "systemProp.$proxy_type.proxyHost=$proxy_host"
          echo "systemProp.$proxy_type.proxyPort=$proxy_port"
        } >> "$GRADLE_USER_HOME/gradle.properties"
      fi
    else
      log_info "No proxy configuration to do for $proxy_type proxy"
    fi
  }

  function handle_no_proxy() {
    proxy_type="$1"
    no_proxy_value=$(echo "$2" | sed -e 's?\s*??g' -e 's?^\.?*.?' -e 's?,\.?,*.?g' -e 's?,?|?g')
    if [[ ! -f  "$GRADLE_USER_HOME/gradle.properties" ]] || ! grep "^systemProp.$proxy_type.nonProxyHosts" "$GRADLE_USER_HOME/gradle.properties"  >/dev/null
    then
      log_info "Installing $proxy_type no proxy for $no_proxy_value"
      {
        echo ""
        echo "systemProp.$proxy_type.nonProxyHosts=$no_proxy_value"
      } >> "$GRADLE_USER_HOME/gradle.properties"
    fi
  }

  function handle_proxies() {
    mkdir -p "$GRADLE_USER_HOME"
    # shellcheck disable=SC2154
    handle_proxy "http" "$http_proxy"
    # shellcheck disable=SC2154
    handle_proxy "https" "$https_proxy"
    # shellcheck disable=SC2154
    handle_no_proxy "http" "$no_proxy"
    # shellcheck disable=SC2154
    handle_no_proxy "https" "$no_proxy"
  }

  function install_ca_certs() {
    certs="${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    if [[ -z "$certs" ]]
    then
      return
    fi

    # import in system
    if echo "$certs" >> /etc/ssl/certs/ca-certificates.crt
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/certs/ca-certificates.crt\\e[0m"
    fi
    if echo "$certs" >> /etc/ssl/cert.pem
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/cert.pem\\e[0m"
    fi

    # import in Java keystore (if keytool command found)
    if command -v keytool > /dev/null
    then
      # shellcheck disable=SC2046
      javahome=${JAVA_HOME:-$(dirname $(readlink -f $(command -v java)))/..}
      # shellcheck disable=SC2086
      keystore=${JAVA_KEYSTORE_PATH:-$(ls -1 $javahome/jre/lib/security/cacerts 2>/dev/null || ls -1 $javahome/lib/security/cacerts 2>/dev/null || echo "")}
      if [[ -f "$keystore" ]]
      then
        storepass=${JAVA_KEYSTORE_PASSWORD:-changeit}
        nb_certs=$(echo "$certs" | grep -c 'END CERTIFICATE')
        log_info "importing $nb_certs certificates in Java keystore \\e[33;1m$keystore\\e[0m..."
        for idx in $(seq 0 $((nb_certs - 1)))
        do
          # TODO: use keytool option -trustcacerts ?
          if echo "$certs" | awk "n==$idx { print }; /END CERTIFICATE/ { n++ }" | keytool -noprompt -import -alias "imported CA Cert $idx" -keystore "$keystore" -storepass "$storepass"
          then
            log_info "... CA certificate [$idx] successfully imported"
          else
            log_warn "... Failed importing CA certificate [$idx]: abort"
            return
          fi
        done
      else
        log_warn "Java keystore \\e[33;1m$keystore\\e[0m not found: could not import CA certificates"
      fi
    fi
  }

  function unscope_variables() {
    _scoped_vars=$(env | awk -F '=' "/^scoped__[a-zA-Z0-9_]+=/ {print \$1}" | sort)
    if [[ -z "$_scoped_vars" ]]; then return; fi
    log_info "Processing scoped variables..."
    for _scoped_var in $_scoped_vars
    do
      _fields=${_scoped_var//__/:}
      _condition=$(echo "$_fields" | cut -d: -f3)
      case "$_condition" in
      if) _not="";;
      ifnot) _not=1;;
      *)
        log_warn "... unrecognized condition \\e[1;91m$_condition\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
      ;;
      esac
      _target_var=$(echo "$_fields" | cut -d: -f2)
      _cond_var=$(echo "$_fields" | cut -d: -f4)
      _cond_val=$(eval echo "\$${_cond_var}")
      _test_op=$(echo "$_fields" | cut -d: -f5)
      case "$_test_op" in
      defined)
        if [[ -z "$_not" ]] && [[ -z "$_cond_val" ]]; then continue; 
        elif [[ "$_not" ]] && [[ "$_cond_val" ]]; then continue; 
        fi
        ;;
      equals|startswith|endswith|contains|in|equals_ic|startswith_ic|endswith_ic|contains_ic|in_ic)
        # comparison operator
        # sluggify actual value
        _cond_val=$(echo "$_cond_val" | tr '[:punct:]' '_')
        # retrieve comparison value
        _cmp_val_prefix="scoped__${_target_var}__${_condition}__${_cond_var}__${_test_op}__"
        _cmp_val=${_scoped_var#"$_cmp_val_prefix"}
        # manage 'ignore case'
        if [[ "$_test_op" == *_ic ]]
        then
          # lowercase everything
          _cond_val=$(echo "$_cond_val" | tr '[:upper:]' '[:lower:]')
          _cmp_val=$(echo "$_cmp_val" | tr '[:upper:]' '[:lower:]')
        fi
        case "$_test_op" in
        equals*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val" ]]; then continue; 
          fi
          ;;
        startswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val"* ]]; then continue; 
          fi
          ;;
        endswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val" ]]; then continue; 
          fi
          ;;
        contains*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val"* ]]; then continue; 
          fi
          ;;
        in*)
          if [[ -z "$_not" ]] && [[ "__${_cmp_val}__" != *"__${_cond_val}__"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "__${_cmp_val}__" == *"__${_cond_val}__"* ]]; then continue; 
          fi
          ;;
        esac
        ;;
      *)
        log_warn "... unrecognized test operator \\e[1;91m${_test_op}\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
        ;;
      esac
      # matches
      _val=$(eval echo "\$${_target_var}")
      log_info "... apply \\e[32m${_target_var}\\e[0m from \\e[32m\$${_scoped_var}\\e[0m${_val:+ (\\e[33;1moverwrite\\e[0m)}"
      _val=$(eval echo "\$${_scoped_var}")
      export "${_target_var}"="${_val}"
    done
    log_info "... done"
  }

  function append_jacoco_task() {
    # shellcheck disable=SC2086
    if $GRADLE_CLI_BIN $GRADLE_CLI_OPTS tasks | grep "$1" >/dev/null && echo "$GRADLE_BUILD_ARGS" | grep -v "$1" >/dev/null
    then
      log_info "Adding $1 task"
      GRADLE_BUILD_ARGS="$GRADLE_BUILD_ARGS $1"
    else
      log_info "--- \\e[32mJaCoCo plugin not   present or $1 already defined in GRADLE_BUILD_ARGS\\e[0m"
    fi
  }

  function output_jacoco_coverage() {
    jacoco_reports=$(find . -name "${JACOCO_CSV_REPORT:-jacocoTestReport.csv}")

    if [[ -n "$jacoco_reports" ]]
    then
      log_info "--- \\e[32mJaCoCo report(s) found\\e[0m (\\e[33;1m${jacoco_reports}\\e[0m): output"
      # shellcheck disable=SC2086
      awk -F',' '{ instructions += $4 + $5; covered += $5 } END { print covered"/"instructions " instructions covered"; print 100*covered/instructions "% covered" }' $jacoco_reports
    else
      log_info "--- \\e[32mJaCoCo report(s) not found: skip"
    fi
  }

  unscope_variables

  # ENDSCRIPT


.gradle-base:
  image: $GRADLE_IMAGE
  # To keep cache across branches add 'key: "$CI_JOB_NAME"'
  cache:
    key: "$CI_COMMIT_REF_SLUG-gradle"
    paths:
      - .gradle
  services:
    - name: "$TBC_TRACKING_IMAGE"
      command: ["--service", "gradle", "1.4.0" ]
  before_script:
    - *gradle-scripts
    - handle_daemon
    - handle_proxies
    - install_ca_certs
    - cd $GRADLE_PROJECT_DIR

gradle-build:
  extends: .gradle-base
  stage: build
  script:
    - append_jacoco_task jacocoTestReport
    - append_jacoco_task jacocoTestCoverageVerification
    - $GRADLE_CLI_BIN $GRADLE_CLI_OPTS $GRADLE_BUILD_ARGS
    - output_jacoco_coverage
  # code coverage RegEx
  coverage: '/^(\d+.\d+\%) covered$/'
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    reports:
      junit:
        - "$GRADLE_PROJECT_DIR/**/build/test-results/test/TEST-*.xml"
    paths:
      - "$GRADLE_PROJECT_DIR/**/build/"

gradle-dependency-check:
  extends: .gradle-base
  stage: test
  dependencies: []
  script:
    - $GRADLE_CLI_BIN ${TRACE+--debug} "$GRADLE_DEPENDENCY_CHECK_TASK"
  rules:
    # on schedule: auto
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      allow_failure: true
      when: always
    # all other cases: manual & non-blocking
    - when: manual
      allow_failure: true
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    paths:
      - "$GRADLE_PROJECT_DIR/**/build/reports/"

.gradle-base-publish:
  extends: .gradle-base
  stage: publish
  script:
    - $GRADLE_CLI_BIN -Pversion=$GRADLE_PUBLISH_VERSION $GRADLE_CLI_OPTS ${GRADLE_PUBLISH_ARGS}

gradle-snapshot:
  extends: .gradle-base-publish
  variables:
    GRADLE_PUBLISH_VERSION: "${CI_COMMIT_REF_SLUG}-SNAPSHOT"
  rules:
    - if: $GRADLE_NO_PUBLISH
      when: never
    # on tags: never
    - if: $CI_COMMIT_TAG
      when: never
    # any manual
    - when: manual
      allow_failure: true

gradle-release:
  extends: .gradle-base-publish
  variables:
    GRADLE_PUBLISH_VERSION: "${CI_COMMIT_REF_SLUG}"
  rules:
    - if: $GRADLE_NO_PUBLISH
      when: never
    # on tags
    - if: $CI_COMMIT_TAG
      when: manual
      allow_failure: true
